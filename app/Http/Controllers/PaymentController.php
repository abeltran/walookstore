<?php

namespace App\Http\Controllers;

use App\Payment;
use Carbon\Carbon;
use Conekta\Conekta;
use Conekta\Order;
use Conekta\Customer;
use Conekta\ProcessingError;
use Conekta\ParameterValidationError;
use Conekta\Handler;
use Illuminate\Support\Facades\Log;
use PDF;

use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $rq)
    {
        Conekta::setApiKey(env('CONEKTA_APP_SECRET'));
        Conekta::setApiVersion("2.0.0");
        Conekta::setLocale('es');

        if(!$this->Validater($rq))
            return response()->json(["success"=>false, "error"=>$this->error]);
        if ($rq->typep=='card') {
            if (!$this->CreateCustomer($rq))
                return response()->json(["success" => false, "error" => $this->error]);
        }
        if ($rq->typep=='oxxo_cash') {
            if(!$this->CreateOxxoOrder($rq))
                return response()->json(["success"=>false, "error"=>$this->error]);
        } else {
            if(!$this->CreateOrder($rq))
                return response()->json(["success"=>false, "error"=>$this->error]);
        }

        $orden = $rq->typep == 'oxxo_cash' ? $this->order->charges[0]->payment_method->reference : $this->order->id;

        $pay = new Payment();
        $pay->qty = 1;
        $pay->product_id = $rq->productid;
        $pay->description = $rq->description;
        $pay->name_card = $rq->nom;
        $pay->number_card = substr($rq->cardnumber, -4);;
        $pay->email = $rq->email;
        $pay->order = $orden;
        $pay->type = $rq->typep;
        $pay->total = $rq->total;
        $pay->phone = $rq->phone;
        $pay->status = $this->order->charges[0]->status;
        $pay->created_at = Carbon::now();
        $pay->save();

        $file = $this->createPDF($orden);

        return response()->json(["success"=>true,"order"=>$this->order,'orderb'=>$orden, "comp"=>$file]);
    }
    public function Validater(Request $rq) {
        if($rq->nom=="" || $rq->total=="" || $rq->email=="" || $rq->phone==""){
            $this->error="El nombre, concepto, télefono, monto y correo electrónico son obligatorios";
            return false;
        }

        if(strlen($rq->cardnumber)<=14 && $rq->typep=='card'){
            $this->error="El número de tarjeta debe tener al menos 15 caracteres";
            return false;
        }
        if(!filter_var($rq->email, FILTER_VALIDATE_EMAIL)){
            $this->error="El correo electrónico no tiene un formato de correo valido";
            return false;
        }
        if($rq->total<=20){
            $this->error="El monto debe ser mayor a 20 pesos";
            return false;
        }

        return true;
    }
    public function CreateCustomer(Request $rq){

        try {
            $this->customer = Customer::create(
                array(
                    "name" => $rq->nom,
                    "email" => $rq->email,
                    "phone" => $rq->phone,
                    "payment_sources" => array(
                        array(
                            "type" => "card",
                            "token_id" => $rq->token
                        )
                    )//payment_sources
                )//customer
            );
        } catch (ProccessingError $error){
            $this->error=$error->getMesage();
            return false;
        } catch (ParameterValidationError $error){
            $this->error=$error->getMessage();
            return false;
        } catch (Handler $error){
            $this->error=$error->getMessage();
            return false;
        }

        return true;
    }
    public function CreateOrder(Request $rq){
        try{

            $this->order = Order::create(
                array(
                    "amount"=>$rq->total,
                    "line_items" => array(
                        array(
                            "name" => $rq->nom,
                            "unit_price" => $rq->total*100,
                            "quantity" => 1
                        )//first line_item
                    ), //line_items
                    "currency" => "MXN",
                    "customer_info" => array(
                        "customer_id" => $this->customer->id
                    ), //customer_info
                    "charges" => array(
                        array(
                            "payment_method" => array(
                                "type" => "default"
                            )
                        ) //first charge
                    ) //charges
                )//order
            );
        } catch (ProcessingError $error){
            $this->error=$error->getMessage();
            return false;
        } catch (ParameterValidationError $error){
            $this->error=$error->getMessage();
            return false;
        } catch (Handler $error){
            $this->error=$error->getMessage();
            return false;
        }

        return true;
    }
    public function CreateOxxoOrder(Request $rq){
        try{
            $thirty_days_from_now = (new \DateTime())->add(new \DateInterval('P30D'))->getTimestamp();

            $this->order = Order::create(
                [
                    "line_items" => [
                        [
                            "name" => $rq->description,
                            "unit_price" => $rq->total*100,
                            "quantity" => 1
                        ]
                    ],
                    "shipping_lines" => [
                        [
                            "amount" => 1500,
                            "carrier" => "FEDEX"
                        ]
                    ], //shipping_lines - physical goods only
                    "currency" => "MXN",
                    "customer_info" => [
                        "name" => $rq->nom,
                        "email" => $rq->email,
                        "phone" => $rq->phone
                    ],
                    "shipping_contact" => [
                        "address" => [
                            "street1" => "Calle 123, int 2",
                            "postal_code" => "06100",
                            "country" => "MX"
                        ]
                    ], //shipping_contact - required only for physical goods
                    "charges" => [
                        [
                            "payment_method" => [
                                "type" => "oxxo_cash",
                                "expires_at" => $thirty_days_from_now
                            ]
                        ]
                    ]
                ]
            );
        } catch (ParameterValidationError $error){
            $this->error->getMessage();
            return false;
        } catch (Handler $error){
            $this->$error->getMessage();
            return false;
        }
        return true;
    }

    public function showReference($reference){
        $pagos = Payment::all();

        $payment= $pagos->where('order',$reference)->where('status','pending_payment')->first();

        return response()->json(['success'=>$payment==null ? false: true, 'data'=>$payment]);
    }
    public function createPDF($order) {

        $pagos = Payment::all();

        $data = $pagos->where('order',$order)->first();
        Log::info(gettype($data));
        Log::info($data->order);
        // share data to view
        view()->share('payment',$data);
        $path = "comp_".$data->order.'.pdf';

        PDF::loadView('pdfOrder', $data)->setPaper('a4', 'landscape')->save('storage/pdf/'.$path);

        return $path;
    }





    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
