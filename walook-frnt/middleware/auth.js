export default function ({ store, error, redirect }) {
    if (!store.$auth.loggedIn) {
      error({
        message: 'You are not connected',
        statusCode: 403
      })
    }else{
      return redirect('/products');
    }
  }
  