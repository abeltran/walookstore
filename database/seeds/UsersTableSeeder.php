<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->name        	 = 'Aldo Beltran';
        $user->email         = 'abeltran@gmail.com';
        $user->password      = 'secret';
        $user->save();

    }
}
