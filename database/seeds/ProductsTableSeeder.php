<?php

use Illuminate\Database\Seeder;
use App\Product;
class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pr = new Product();
        $pr->name = 'EVENTO SUBASTA CASAS';
        $pr->qty = 1;
        $pr->price = 75;
        $pr->image = 'https://images.pexels.com/photos/210617/pexels-photo-210617.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260';
        $pr->description = 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Laborum magni, nemo vitae facere, rem ex, doloremque laboriosam iste expedita tempora quod commodi. Maiores eum amet maxime quod ea expedita esse.';
        $pr->save();

        $pr = new Product();
        $pr->name = 'EVENTO SUBASTA AUTOS';
        $pr->qty = 1;
        $pr->price = 95;
        $pr->image = 'https://images.pexels.com/photos/175568/pexels-photo-175568.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260';
        $pr->description = 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Laborum magni, nemo vitae facere, rem ex, doloremque laboriosam iste expedita tempora quod commodi. Maiores eum amet maxime quod ea expedita esse.';
        $pr->save();

        $pr = new Product();
        $pr->name = 'EVENTO BRUNCH';
        $pr->qty = 1;
        $pr->price = 55;
        $pr->image = 'https://cdn.vuetifyjs.com/images/cards/cooking.png';
        $pr->description = 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Laborum magni, nemo vitae facere, rem ex, doloremque laboriosam iste expedita tempora quod commodi. Maiores eum amet maxime quod ea expedita esse.';

        $pr->save();
    }
}
