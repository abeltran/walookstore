<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ORDER</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<h1 class="text-center">Comprobante de Pago</h1>
<table class="table table-bordered table-sm">
    <thead>
    <tr class="table-info">
        <td>Producto</td>
        <td>Cliente</td>
        <td>Cantidad</td>
        <td>Email</td>
        <td>Télefono</td>
        <td>Tipo Pago</td>
        <td># Orden</td>
        <td>Total</td>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{ $payment->description}}</td>
            <td>{{ $payment->name_card }}</td>
            <td>{{ $payment->qty }}</td>
            <td>{{ $payment->email }}</td>
            <td>{{ $payment->phone }}</td>
            <td>{{ $payment->type == 'card' ? 'TARJETA' : 'OXXO' }}</td>
            <td>{{ $payment->order }}</td>
            <td>$ {{ $payment->total}}</td>
        </tr>
    </tbody>
</table>
</body>
</html>
